import {
  ConnectedSocket,
  MessageBody,
  OnGatewayConnection,
  OnGatewayDisconnect,
  SubscribeMessage,
  WebSocketGateway,
} from '@nestjs/websockets';
import { Socket } from 'socket.io';
import { Point } from './types/point.type';

@WebSocketGateway({
  cors: {
    origin: '*',
  },
})
export class AppGateway implements OnGatewayConnection, OnGatewayDisconnect {
  handleConnection(@ConnectedSocket() socket: Socket): void {
    console.log('A player has connected');
  }

  handleDisconnect(@ConnectedSocket() socket: Socket): void {
    console.log('A player has disconnected');
  }

  @SubscribeMessage('FIRST_POINT_FROM_PLAYER')
  start(@ConnectedSocket() socket: Socket, @MessageBody() point: Point): void {
    socket.broadcast.emit('FIRST_POINT_TO_PLAYERS', point);
  }

  @SubscribeMessage('POINT_FROM_PLAYER')
  addPoint(
    @ConnectedSocket() socket: Socket,
    @MessageBody() point: Point,
  ): void {
    socket.broadcast.emit('POINT_TO_PLAYERS', point);
  }
}

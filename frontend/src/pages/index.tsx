import { LineProvider } from "@/components/line-provider";
import { WebSocketProvider } from "@/components/websocket-provider";
import dynamic from "next/dynamic";

const Sketch = dynamic(
	() => import("../components/sketch").then((mod) => mod.Sketch),
	{
		ssr: false,
	}
);

export default function Home() {
	return (
		<LineProvider>
			<WebSocketProvider>
				<Sketch />
			</WebSocketProvider>
		</LineProvider>
	);
}

import { LinesData } from "@/types/lines-data"; 
import { Point } from "@/types/point"; 
import { ReactNode, createContext, useContext, useReducer } from "react";

type ActionName = "ADD_POINT" | "ADD_FIRST_POINT"; 

export type Action = { type: ActionName; point: Point }; 
export type Dispatch = (action: Action) => void; 
export type State = LinesData; 

const LineStateContext = createContext<
	{ state: State; dispatch: Dispatch } | undefined
>(undefined); 

function useLines() { 
	const context = useContext(LineStateContext);
	if (context === undefined) {
		throw new Error("useLines must be used within a LineProvider");
	}
	return context;
}

function lineReducer(state: State, action: Action): State { 
	const { type, point } = action;

	switch (type) {
		case "ADD_FIRST_POINT": {
			return [...state, [point.x, point.y]];
		}
		case "ADD_POINT": {
			const lines = [...state];

			let [lastLine] = lines.splice(-1);
			return [...lines, [...lastLine, point.x, point.y]];
		}
		default: {
			throw new Error(`Unhandled action type: ${type}`);
		}
	}
}

function LineProvider({ children }: { children: ReactNode }) { 
	const [state, dispatch] = useReducer(lineReducer, []); 

	return (
		<LineStateContext.Provider value={{ state, dispatch }}> 
			{children}
		</LineStateContext.Provider>
	);
}

export { LineProvider, useLines };

import { createContext, useContext, useEffect, useMemo } from "react";
import { Socket, io } from "socket.io-client";
import { useLines } from "@/components/line-provider";
import { Point } from "@/types/point";
import getConfig from "next/config";
const { publicRuntimeConfig } = getConfig();
const { BACKEND_URL } = publicRuntimeConfig;  

type State = { 
	emitFirstPointFromPlayer: Function,
	emitPointFromPlayer: Function
};

const WebSocketContext = createContext<State | undefined>(undefined);

function WebSocketProvider({ children }: { children: React.ReactNode }): JSX.Element {
	const socket: Socket = useMemo(
		() => io(BACKEND_URL, { autoConnect: false }),
		[]
	);
	const { dispatch } = useLines();

	useEffect(() => {
		if (!socket.connected) {
			socket.connect();
		}
		return () => {
			socket.close();
		};
	}, [socket]);

	useEffect(() => { 
		if (!socket) return;

		socket.on("FIRST_POINT_TO_PLAYERS", (point: Point) => { 
			dispatch({ type: "ADD_FIRST_POINT", point: point });
		});

		socket.on("POINT_TO_PLAYERS", (point: Point) => { 
			dispatch({ type: "ADD_POINT", point: point });
		});
	}, [socket, dispatch]);

	const emitFirstPointFromPlayer = (point: Point) => { 
		socket.emit("FIRST_POINT_FROM_PLAYER", point);
	};

	const emitPointFromPlayer = (point: Point) => { 
		socket.emit("POINT_FROM_PLAYER", point);
	};

	return (
		<WebSocketContext.Provider value={{ emitFirstPointFromPlayer, emitPointFromPlayer }}>
			{children}
		</WebSocketContext.Provider>
	);
}

function useWebSocket(): State {
	const context = useContext(WebSocketContext);
	if (context === undefined) {
		throw new Error("useWebSocket must be used within a WebSocketProvider");
	}
	return context;
}

export { WebSocketProvider, useWebSocket };

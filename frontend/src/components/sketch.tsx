import React from "react";
import Konva from "konva";
import { Stage, Layer, Line } from "react-konva";
import { useLines } from "@/components/line-provider";
import { useWebSocket } from "@/components/websocket-provider";
import getConfig from "next/config";
const { publicRuntimeConfig } = getConfig();
const {
	STROKE_COLOR,
	STROKE_THICKNESS
} : { STROKE_COLOR: string; STROKE_THICKNESS: number } = publicRuntimeConfig; 

export const Sketch = () => {
	const { state: lines, dispatch: dispatchLines } = useLines();
	const { emitFirstPointFromPlayer, emitPointFromPlayer } = useWebSocket();
	const isDrawing = React.useRef(false);

	const getPointFromMouseEvent = (
		mouseEvent: Konva.KonvaEventObject<MouseEvent>
	) => {
		return mouseEvent.target.getStage()?.getPointerPosition() ?? { x: 0, y: 0 };
	};

	const handleMouseDown = (e: Konva.KonvaEventObject<MouseEvent>) => {
		isDrawing.current = true;
		const point = getPointFromMouseEvent(e);
		dispatchLines({ type: "ADD_FIRST_POINT", point: point });
		emitFirstPointFromPlayer(point);
	};

	const handleMouseMove = (e: Konva.KonvaEventObject<MouseEvent>) => {
		if (!isDrawing.current) {
			return;
		}
		const point = getPointFromMouseEvent(e);
		dispatchLines({ type: "ADD_POINT", point: point });
		emitPointFromPlayer(point);
	};

	const handleMouseUp = () => {
		isDrawing.current = false;
	};

	return (
		<div>
			<Stage
				width={window.innerWidth}
				height={window.innerHeight}
				onMouseDown={handleMouseDown}
				onMousemove={handleMouseMove}
				onMouseup={handleMouseUp}
			>
				<Layer>
					{lines.map((line, i) => (
						<Line
							key={i}
							points={line}
							stroke={STROKE_COLOR}
							strokeWidth={STROKE_THICKNESS}
							tension={0.5}
							lineCap="round"
							lineJoin="round"
						/>
					))}
				</Layer>
			</Stage>
		</div>
	);
};

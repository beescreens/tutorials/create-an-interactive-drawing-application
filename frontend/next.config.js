/** @type {import('next').NextConfig} */
const nextConfig = {
	reactStrictMode: true,
	publicRuntimeConfig: {
		BACKEND_URL: process.env.BACKEND_URL,
		STROKE_COLOR: process.env.STROKE_COLOR,
		STROKE_THICKNESS: process.env.STROKE_THICKNESS,
	},
}

module.exports = nextConfig
